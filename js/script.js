//Text slider
$(function() {
  var $carouselList = $("#carousel ul");

  setInterval(changeSlides, 5000);

  function changeSlides() {
    $carouselList.animate({ marginLeft: -500 }, 2000, moveFirstSlide);
  }

  function moveFirstSlide() {
    var $firstItem = $carouselList.find("li:first");
    var $lastItem = $carouselList.find("li:last");
    $lastItem.after($firstItem);
    $carouselList.css({ marginLeft: 0 });
  }
});

//Gallery slider

$(function() {
  var $carouselGalleryList = $("#carousel-container ul");

  setInterval(changeSlides, 4000);

  function changeSlides() {
    $carouselGalleryList.animate({ marginLeft: -370 }, 3000, moveFirstSlide);
  }

  function moveFirstSlide() {
    var $firstGalleryItem = $carouselGalleryList.find("li:first");
    var $lastGalleryItem = $carouselGalleryList.find("li:last");
    $lastGalleryItem.after($firstGalleryItem);
    $carouselGalleryList.css({ marginLeft: 0 });
  }
});
